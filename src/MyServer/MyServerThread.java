package MyServer;

import java.io.*;
import java.net.Socket;

public class MyServerThread extends Thread {
    Socket socket;

    public MyServerThread(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {

        try (InputStream inputStream = socket.getInputStream()) {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            OutputStream outputStream = socket.getOutputStream();
            PrintWriter printWriter = new PrintWriter(outputStream);

            String text;
            while ((text = bufferedReader.readLine()) != null) {
                    printWriter.println("New client: " + text);
                    printWriter.flush();

                if (text.equalsIgnoreCase("bye")) {
                    new MyServerThread(socket).interrupt();
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}


// try (InputStream inputStream = socket.getInputStream()) {
//         BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//
//         OutputStream outputStream = socket.getOutputStream();
//         PrintWriter printWriter = new PrintWriter(outputStream);
//
//         String text = bufferedReader.readLine();
//         do {
//         printWriter.println("Server: " + text);
//         printWriter.flush();
//
//         } while (!text.equalsIgnoreCase("bye"));
//         new MyServerThread(socket).interrupt();