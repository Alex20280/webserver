package MyServer;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class SomeClient {
    public static final int PORT = 1016;

    public static void main(String[] args) {

        try (Socket socket = new Socket("localhost", PORT)) {
            OutputStream output = socket.getOutputStream();
            PrintWriter writer = new PrintWriter(output, true);

            Scanner scanner = new Scanner(System.in);
            String text;

            while (scanner.hasNext()) {
                do {
                    text = scanner.nextLine();
                    writer.println(text);
                    writer.flush();

                    InputStream input = socket.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    String info = reader.readLine();
                    System.out.println(info);

                } while (!text.equalsIgnoreCase("bye"));
                {
                    System.out.println("-----------------");
                    System.out.println("Session is closed");
                }
            }

        } catch (UnknownHostException ex) {
            System.out.println("Server not found: " + ex.getMessage());
        } catch (IOException ex) {
            System.out.println("I/O error: " + ex.getMessage());
        }
    }
}

// try (Socket socket = new Socket("localhost", PORT)) {
//         OutputStream output = socket.getOutputStream();
//         PrintWriter writer = new PrintWriter(output, true);
//
//         Scanner scanner = new Scanner(System.in);
//         String text;
//
//         do {
//         text = scanner.nextLine();
//         writer.println(text);
//         writer.flush();
//
//         InputStream input = socket.getInputStream();
//         BufferedReader reader = new BufferedReader(new InputStreamReader(input));
//         String info = reader.readLine();
//         System.out.println(info);
//
//         } while (!text.equalsIgnoreCase("bye"));{
//         System.out.println("-----------------");
//         System.out.println("Session is closed");
//        }