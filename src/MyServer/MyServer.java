package MyServer;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class MyServer {

    public static final int PORT = 1016;

    public static void main(String[] args) {

        try (ServerSocket serverSocket = new ServerSocket(PORT)) {
            System.out.println("Server connected and listening to port " + PORT);

            while (true) {
                Socket socket = serverSocket.accept();
                System.out.println("New client connected");
                new MyServerThread(socket).start();
                System.out.print("Number of active clients: ");
                System.out.println(MyServerThread.activeCount() - 2);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
